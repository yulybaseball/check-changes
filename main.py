#!/bin/env python

import check_changes as cg
from email import email

def prepare_email_data(data):
    subject = 'PSS Consoles Source Code checking'
    body = 'No change was detected.'
    if data['new'] or data['removed'] or data['updated']:
        subject = 'ALERT - Changes detected: ' + subject
        body = 'NEW items\n'
        for new_item in data['new']:
            body += ('\t' + new_item + '\n')
        body += ('\nREMOVED items\n')
        for removed_item in data['removed']:
            body += ('\t' + removed_item + '\n')
        body += ('\nUPDATED items\n')
        for updated_item in data['updated']:
            body += ('\t' + updated_item + '\n')
    return subject,body

def send_email(data):
    subject, body = prepare_email_data(data)
    email.send_email(subject, body)

def main():
    cg.start()
    # load included items
    inc_items = cg.load_included_items(verbose=True)
    
    # load excluded items
    exc_items = cg.load_discarded_items(verbose=True)

    # get initial files status
    initial_items_status = cg.get_initial_status(inc_items, exc_items)

    # compute 'real' items for comparisson
    items2compare = cg.get_items2compare(inc_items, exc_items)

    # get missing, new and edited files
    data = cg.compute_diff(initial_items_status, items2compare)

    # send email
    send_email(data)

    # finish... just in case it was not called before
    cg.quit_script()

if __name__ == "__main__":
    main()