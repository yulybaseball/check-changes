import datetime
import os
import time

from logger.logger import load_logger

INCLUDED_DIRS_FILE_NAME = os.path.join(os.path.dirname(
                            os.path.realpath(__file__)), 'included_items.txt')
DISCARDED_DIRS_FILE_NAME =  os.path.join(os.path.dirname(
                            os.path.realpath(__file__)), 'discarded_items.txt')
INITIAL_STATUS_FILE_NAME =  os.path.join(os.path.dirname(
                            os.path.realpath(__file__)), 'initial_status.txt')
LOG_FILE_NAME = os.path.join(os.path.dirname(
                            os.path.realpath(__file__)), 'check_changes.log')

logger = None

def quit_script():
    """Print time and exit current script."""
    logger.info('Quiting now...')
    logger.info(datetime.datetime.now())
    logger.info('============================================================')
    exit() 

def start():
    """Receive the logger object and print the initial time to log file."""
    global logger
    logger = load_logger(log_file_name=LOG_FILE_NAME)
    logger.info('============================================================')
    logger.info(datetime.datetime.now())

def load_included_items(verbose=False):
    """
    Searches the list of directories and files to include in the comparisson.
    Will search for the file INCLUDED_DIRS_FILE_NAME and include every
    entry in the file.
    """
    included_items = []
    try:
        included_items = [line.rstrip('\n') 
            for line in open(INCLUDED_DIRS_FILE_NAME) 
                if os.path.exists(line.rstrip('\n'))]
        if not included_items:
            logger.error('There are no items to include in the comparisson.')
            quit_script()

        if verbose:
            logger.info("Directories and files included:\n{0}".\
                format("\t{0}".format('\n\t'.join(map(str, included_items)))))

    except IOError:
        logger.error("File " + INCLUDED_DIRS_FILE_NAME + " was not found!!")
        quit_script()

    return included_items

def load_discarded_items(verbose=False):
    """
    Searches the list of directories and files to discard in the comparisson.
    Will search for the file DISCARDED_DIRS_FILE_NAME and include every
    entry in the file.
    """
    discarded_items = []
    try:
        discarded_items = [line.rstrip('\n') 
            for line in open(DISCARDED_DIRS_FILE_NAME) 
                if os.path.exists(line.rstrip('\n'))]

        if verbose:
            logger.info("Directories and files excluded:\n{0}".\
                format("\t{0}".format('\n\t'.join(map(str, discarded_items)))))

    except IOError:
        logger.warning(DISCARDED_DIRS_FILE_NAME + ' was not found!!')
    return discarded_items

def compute_dir_index(items):
    """
    Return a tuple containing:
        - list of files (relative to path)
        - lisf of subdirs (relative to path)
        - a dict: filepath => last
    Path will be retrieve from every item in items list.

    Keyword arguments:
    items -- List containing paths.
    """
    files = []
    subdirs = []
    index = {}

    for line in items:
        for root, dirs, filenames in os.walk(line):
            for subdir in dirs:
                subdirs.append(os.path.abspath(os.path.join(root, subdir)))

            for f in filenames:
                files.append(os.path.abspath(os.path.join(root, f)))

        for f in files:
            index[f] = os.path.getmtime(os.path.join(line, files[0]))

    return dict(files=files, subdirs=subdirs, index=index)

def get_items2compare(inc_items, exc_items):
    """Create and return a dictionary with the files needed for comparisson.

    Keyword arguments:
    inc_items -- list of dirs and files to be included, as per conf file.
    exc_items -- list of dirs and files to be excluded, as per conf file.
    """
    items2compare = {}
    in_its_d = compute_dir_index(inc_items)
    ex_its_d = compute_dir_index(exc_items)
    # items to compare
    items2compare = list(set(in_its_d['files']) - set(ex_its_d['files']))
    # need this step because os.walk() doesn't handle files
    # and excluded items list might have files specified in it
    return list(set(items2compare) - set(exc_items))

def create_initial_status_file(included_items, excluded_items):
    """
    Create initial status file containing the name of every file to be included
    in the comparisson and the timestamp the file was modified. It has 
    this format:
        full_file_path  [tab]   update_timestamp
    """
    with open(INITIAL_STATUS_FILE_NAME, 'w+') as init_stat_file:
        for line in included_items:
            for root, dirs, filenames in os.walk(line):
                # exclude the dirs in excluded_items list
                dirs[:] = [d for d in dirs if (root + '/' + d) not in excluded_items]
                for f in filenames:
                    temp_file = os.path.abspath(os.path.join(root, f))
                    if temp_file not in excluded_items:
                        init_stat_file.write(temp_file + '\t' + \
                        str(os.path.getmtime(temp_file)) + '\n')

def get_initial_status(included_items, excluded_items):
    """
    Create the initial status file: index to compare to when checking 
    "original" files' update time. Exit the script.
    If file already exists, create and return a dictionary with the complete 
    path and file name, and a representation number of the files' update 
    time.

    Keyword arguments:
    included_items -- List with items to be included for comparisson.
    excluded_items -- List with items to be excluded from comparisson.
    """
    # if file doesn't exist, create it
    try:
        if not os.path.exists(INITIAL_STATUS_FILE_NAME):
            create_initial_status_file(included_items, excluded_items)
            # in this case, we don't need to compare: initial status 
            # just was set up, so we can exit script
            logger.warning(INITIAL_STATUS_FILE_NAME + 
                          ' didn\'t exist: it was created and populated.')
            quit_script()
        # file exists... create and return dictionary with files and timestamps
        initial_status = {}
        with open(INITIAL_STATUS_FILE_NAME, 'r') as init_stat_file:
            for line in init_stat_file:
                file_entry = line.split('\t')
                initial_status[file_entry[0]] = file_entry[1].replace('\n', '')
        return initial_status
    except IOError as err:
        logger.error(err)
        quit_script()

def log_diff(data):
    if data['new'] or data['removed'] or data['updated']:
        logger.warning('AT LEAST ONE CHANGE WAS DETECTED:')
        log = 'NEW items\n'
        for new_item in data['new']:
            log += ('\t' + new_item + '\n')
        log += ('\nREMOVED items\n')
        for removed_item in data['removed']:
            log += ('\t' + removed_item + '\n')
        log += ('\nUPDATED items\n')
        for updated_item in data['updated']:
            log += ('\t' + updated_item + '\n')
        logger.info(log)
    else:
        logger.info('NO CHANGES WERE DETECTED.')

def compute_diff(initial_items_status, items2compare):
    """
    Compute the updated, new and removed files.
    Return a dictionary with those keys and lists as values: 
        data => {'updated' : [], 'new' : [], 'removed': []}

    Keyword arguments:
    initial_items_status -- Dict with the initial status for files and times.
    items2compare -- List containing the full path to files to be checked 
        on the comparisson.
    """
    ini_st_files = list(initial_items_status.keys())
    new_items = list(set(items2compare) - set(ini_st_files))
    removed_items = list(set(ini_st_files) - set(items2compare))
    data = {}
    data['updated'] = []
    for f in set(items2compare).intersection(set(ini_st_files)):
        if initial_items_status[f] != str(os.path.getmtime(f)):
            data['updated'].append(f)
    data['new'] = new_items
    data['removed'] = removed_items
    log_diff(data)
    return data
